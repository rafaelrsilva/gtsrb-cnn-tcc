# -*- coding: utf-8 -*-

import io, json, sys, os
reload(sys)
sys.setdefaultencoding('utf8')
sys.dont_write_bytecode = True

import gtsrb
from gtsrb import dataset as GTSRBDataset
from gtsrb import GTSRBConvNet
from gtsrb import keras

x_train = y_train = x_test = y_test = None

def loadDatasets(adjustDatasetImages = False, imageWidth = 40, imageHeight = 40):
    (xtrain, ytrain), (xtest, ytest) = GTSRBDataset.load(adjustDatasetImages, imageWidth, imageHeight)

    global x_train; x_train = xtrain
    global y_train; y_train = ytrain
    global x_test; x_test = xtest
    global y_test; y_test = ytest

def isDatasetLoaded():
    if(x_train is None or y_train is None or x_test is None or y_test is None):
        print('Dataset não carregado')
        return False
    
    return True

def testarConfiguracoesDeConvNet(epochs):
    if(not isDatasetLoaded()):
        return False

    SGDLearningRatesValues = [0.001, 0.01, 0.1]
    SGDMomentumValues      = [0, 0.3, 0.6, 0.9]
    SGDNesterovValues      = [False, True]

    ADAMLearningRatesValues = SGDLearningRatesValues
    ADAMBeta1Values         = [0.9, 0.99, 0.999]
    ADAMBeta2Values         = [0.9, 0.99, 0.999]

    resultados = {'adam': [], 'sgd': []}

    for lr in SGDLearningRatesValues:
        for momentum in SGDMomentumValues:
            for nesterov in SGDNesterovValues:
                print '\n\nTreinando com SGD: lr = {lr}, momentum = {momentum}, nesterov = {nesterov}\n'.format(lr = lr, momentum = momentum, nesterov = nesterov)

                gtsrbConvNet = GTSRBConvNet(img_x = 40, img_y = 40)
                
                (hist, logs) = gtsrbConvNet.train(x_train, y_train, x_test, y_test, epochs = epochs, optimizer = keras.optimizers.SGD(lr = lr, momentum = momentum, nesterov = nesterov))
                
                resultados['sgd'].append({
                    'lr'       : lr,
                    'momentun' : momentum,
                    'nesterov' : int(nesterov),
                    'epochs'   : logs,
                    'acc'      : logs[len(logs) - 1]['acc'],
                    'loss'     : logs[len(logs) - 1]['loss'],
                    'val_acc'  : logs[len(logs) - 1]['val_acc'],
                    'val_loss' : logs[len(logs) - 1]['val_loss'],
                })

    for lr in ADAMLearningRatesValues:
        for beta_1 in ADAMBeta1Values:
            for beta_2 in ADAMBeta2Values:
                print '\n\nTreinando com ADAM: lr = {lr}, beta_1 = {beta_1}, beta_2 = {beta_2}\n'.format(lr = lr, beta_1 = beta_1, beta_2 = beta_2)

                gtsrbConvNet = GTSRBConvNet(img_x = 40, img_y = 40)
                
                (hist, logs) = gtsrbConvNet.train(x_train, y_train, x_test, y_test, epochs = epochs, optimizer = keras.optimizers.Adam(lr = lr, beta_1 = beta_1, beta_2 = beta_2))
                
                resultados['adam'].append({
                    'lr'       : lr,
                    'beta_1'   : beta_1,
                    'beta_2'   : beta_2,
                    'epochs'   : logs,
                    'acc'      : logs[len(logs) - 1]['acc'],
                    'loss'     : logs[len(logs) - 1]['loss'],
                    'val_acc'  : logs[len(logs) - 1]['val_acc'],
                    'val_loss' : logs[len(logs) - 1]['val_loss'],
                })

    resultados['adam'] = sorted(resultados['adam'], key=lambda adam: adam['val_acc'], reverse=True)
    resultados['sgd'] = sorted(resultados['sgd'], key=lambda sgd: sgd['val_acc'], reverse=True)

    return resultados

def exportarResultadosConfiguracoes(epochs = 10):
    if(not isDatasetLoaded()):
        return False

    with io.open('resultados.json', 'w', encoding = 'utf-8') as file:
        file.write(unicode(json.dumps(testarConfiguracoesDeConvNet(epochs), ensure_ascii = False, indent = 4)))

def exportarDadosMatrizConfusao(trainModel = False, modelFilePath = 'model-best.h5'):
    if(not isDatasetLoaded()):
        return False

    if(not trainModel and modelFilePath and os.path.exists(modelFilePath)):
        gtsrbConvNet = GTSRBConvNet(img_x = 40, img_y = 40, modelFilePath = 'model-best.h5')

    else:
        gtsrbConvNet = GTSRBConvNet(img_x = 40, img_y = 40)        

    if(trainModel):
        modelCheckpointCallback = keras.callbacks.ModelCheckpoint(modelFilePath, monitor = 'val_acc', verbose = 1, save_best_only = True, mode = 'max', period = 1)

        (hist, logs) = gtsrbConvNet.train(x_train, y_train, x_test, y_test, epochs = 10, optimizer = keras.optimizers.Adam(lr = 0.001, beta_1 = 0.9, beta_2 = 0.999), callbacks = [modelCheckpointCallback])
    
    with io.open('predict.json', 'w', encoding = 'utf-8') as file:
        file.write(unicode(json.dumps(gtsrbConvNet.predict(x_test), ensure_ascii = False, indent = 4)))

def montarMatrizConfusao(log = False):
    if(not isDatasetLoaded()):
        return False

    classes = [[0 for j in range(43)] for i in range(43)]

    with open('predict.json', 'r') as predictJsonFile:
        predicts = json.load(predictJsonFile)

        if(log):
            count = 0;
            erros = 0;

        for index, predict in enumerate(predicts):
            numeroClasseCorreta = int(y_test[index]);
            numeroClassePrevista = False
            porcentagemPrevista = False

            for numeroClasseAtual, porcentagemPrevistaAtual in enumerate(predict):
                porcentagemPrevistaAtual = float(porcentagemPrevistaAtual)
                
                if((numeroClassePrevista is False and porcentagemPrevista is False) or porcentagemPrevistaAtual > porcentagemPrevista):
                    numeroClassePrevista = numeroClasseAtual
                    porcentagemPrevista = porcentagemPrevistaAtual

            if(log):

                if(numeroClasseCorreta != numeroClassePrevista):
                    erros = erros + 1;

                    diferencaParaClasseCorreta = porcentagemPrevista - predict[numeroClasseCorreta]
                    print('Correto: {correto} ({porcentagemCorreto}) | Previsto: {previsao} ({porcentagemPrevisao})'.format(correto = numeroClasseCorreta, porcentagemCorreto = predict[numeroClasseCorreta], previsao = numeroClassePrevista, porcentagemPrevisao = porcentagemPrevista))

                count = count + 1;

            classes[numeroClasseCorreta][numeroClassePrevista] = classes[numeroClasseCorreta][numeroClassePrevista] + 1;

        if(log):
            print('Total Avaliado: {totalAvaliado} | Total Erros: {totalErros}'.format(totalAvaliado = count, totalErros = erros))

    with io.open('matriz.json', 'w', encoding = 'utf-8') as file:
        file.write(unicode(json.dumps(classes, ensure_ascii = False, indent = 4)))


def gerarGraficosDosResultados(resultadosFileName = 'resultados.json'):
    import matplotlib

    matplotlib.use('Agg')

    import matplotlib.pyplot as plt

    with open(resultadosFileName, 'r') as resultadosFile:
        resultados = json.load(resultadosFile)

        bestSgdIndex = None
        bestSgdEpoch = None
        
        bestAdamIndex = None
        bestAdamEpoch = None

        for sgdIndex, sgdResultado in enumerate(resultados['sgd']):
            for epochIndex, epoch in enumerate(sgdResultado['epochs']):
                if(not bestSgdIndex and not bestSgdEpoch or epoch['val_acc'] > resultados['sgd'][bestSgdIndex]['epochs'][bestSgdEpoch]['val_acc']):
                    bestSgdIndex = sgdIndex
                    bestSgdEpoch = epochIndex
            

        for adamIndex, adamResultado in enumerate(resultados['adam']):
            for epochIndex, epoch in enumerate(adamResultado['epochs']):
                if(not bestAdamIndex and not bestAdamEpoch or epoch['val_acc'] > resultados['adam'][bestAdamIndex]['epochs'][bestAdamEpoch]['val_acc']):
                    bestAdamIndex = adamIndex
                    bestAdamEpoch = epochIndex


        bestSgd = resultados['sgd'][bestSgdIndex]
        bestAdam = resultados['adam'][bestAdamIndex]

        bestSgd['acc']      = [epoch.get('acc') for epoch in bestSgd['epochs']] 
        bestSgd['loss']     = [epoch.get('loss') for epoch in bestSgd['epochs']] 
        bestSgd['val_acc']  = [epoch.get('val_acc') for epoch in bestSgd['epochs']] 
        bestSgd['val_loss'] = [epoch.get('val_loss') for epoch in bestSgd['epochs']] 

        bestAdam['acc']      = [epoch.get('acc') for epoch in bestAdam['epochs']] 
        bestAdam['loss']     = [epoch.get('loss') for epoch in bestAdam['epochs']] 
        bestAdam['val_acc']  = [epoch.get('val_acc') for epoch in bestAdam['epochs']] 
        bestAdam['val_loss'] = [epoch.get('val_loss') for epoch in bestAdam['epochs']] 

        def montarGrafico(rangeGrafico, data1, label1, data2, label2, xlabel, ylabel, fileName, title = ''):
            fig = plt.figure()
            
            ax = plt.subplot(111)
            ax.plot(rangeGrafico, data1, label = label1)
            ax.plot(rangeGrafico, data2, label = label2)
            ax.tick_params(axis = 'both', which = 'major', pad = 5)
            ax.legend(loc = 'center left', bbox_to_anchor = (1, 0.5), fancybox = False, shadow = False)

            plt.suptitle(title, fontsize = 9)
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)

            plt.savefig(fileName, format = 'png', dpi = 300, bbox_inches = 'tight')

        rangeGraficos = range(1, 11)

        # Comparação de acurácia entre os algoritmos durante o treinamento
        montarGrafico(rangeGraficos, bestSgd['acc'], 'SGD', bestAdam['acc'], 'ADAM', 'Epoch', 'Accuracy', 'acuracia-treinamento.png', 'SGD x ADAM - Accuracy - Training')

        # Comparação de erro/perda entre os algoritmos durante o treinamento
        montarGrafico(rangeGraficos, bestSgd['loss'], 'SGD', bestAdam['loss'], 'ADAM', 'Epoch', 'Loss', 'erro-treinamento.png', 'SGD x ADAM - Loss - Training')

        # Comparação de acurácia entre os algoritmos durante a validação
        montarGrafico(rangeGraficos, bestSgd['val_acc'], 'SGD', bestAdam['val_acc'], 'ADAM', 'Epoch', 'Accuracy', 'acuracia-validacao.png', 'SGD x ADAM - Accuracy - Validation\nSGD Best Epoch = {sgdEpoch}\nADAM Best Epoch = {adamEpoch}\n'.format(sgdEpoch = bestSgdEpoch + 1, adamEpoch = bestAdamEpoch + 1))

        # Comparação de erro/perda entre os algoritmos durante a validação
        montarGrafico(rangeGraficos, bestSgd['val_loss'], 'SGD', bestAdam['val_loss'], 'ADAM', 'Epoch', 'Loss', 'erro-validacao.png', 'SGD x ADAM - Loss - Validation')

        # Comparação de acurácia entre o treinamento e a validação para o algoritmo SGD
        montarGrafico(rangeGraficos, bestSgd['acc'], 'Training', bestSgd['val_acc'], 'Validation', 'Epoch', 'Accuracy', 'algoritmo-sgd-acuracia.png', 'Training x Validation - SGD - Accuracy')

        # Comparação de erro/perda entre o treinamento e a validação para o algoritmo SGD
        montarGrafico(rangeGraficos, bestSgd['loss'], 'Training', bestSgd['val_loss'], 'Validation', 'Epoch', 'Loss', 'algoritmo-sgd-erro.png', 'Training x Validation - SGD - Loss')

        # Comparação de acurácia entre o treinamento e a validação para o algoritmo ADAM
        montarGrafico(rangeGraficos, bestAdam['acc'], 'Training', bestAdam['val_acc'], 'Validation', 'Epoch', 'Accuracy', 'algoritmo-adam-acuracia.png', 'Training x Validation - ADAM - Accuracy')

        # Comparação de erro/perda entre o treinamento e a validação para o algoritmo ADAM
        montarGrafico(rangeGraficos, bestAdam['loss'], 'Training', bestAdam['val_loss'], 'Validation', 'Epoch', 'Loss', 'algoritmo-adam-erro.png', 'Training x Validation - ADAM - Loss')

