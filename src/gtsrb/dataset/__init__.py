# -*- coding: utf-8 -*-

import os, numpy, csv, matplotlib

matplotlib.use('Agg')

import matplotlib.pyplot as plt
from skimage import transform

classes = 43

def _getTestDatasetPath(path = ''):
	return os.path.dirname(__file__) + '/test/images/' + path.strip()

def _getTrainingDatasetPath(path = ''):
	return os.path.dirname(__file__) + '/training/images/' + path.strip()

def _getResizedImage(image, width, height):
    return transform.resize(image, (width, height))

def _getCroppedImage(image, x1, y1, x2, y2):
    return image[y1:y2, x1:x2];

def _getAdjustedImage(image, width, height, x1, y1, x2, y2):
    image = _getCroppedImage(image, int(x1), int(y1), int(x2), int(y2))
    image = _getResizedImage(image, width, height)
    return image

def _loadTrainingDataset(adjustDatasetImages, imageWidth, imageHeight):
    images = []
    labels = []

    for classId in range(0, classes):
    	classId   = format(classId, '05d')
    	classPath = _getTrainingDatasetPath(classId)
    	classFile = classPath + '/' + 'GT-' + classId + '.csv'

        with open(classFile) as gtFile:
            gtReader = csv.reader(gtFile, delimiter = ';')
            gtReader.next()

            for index, row in enumerate(gtReader):
                image = plt.imread(classPath + '/' + row[0])

                if(adjustDatasetImages):
                    image = _getAdjustedImage(image, imageWidth, imageHeight, row[3], row[4], row[5], row[6])
                    plt.imsave(classPath + '/' + row[0], image)

                images.append(image[:,:,:3])
                labels.append(row[7])

    return (numpy.array(images), labels)

def _loadTestDataset(adjustDatasetImages, imageWidth, imageHeight):
    images = []
    labels = []

    with open(_getTestDatasetPath('GT-final_test.csv')) as gtFile:
        gtReader = csv.reader(gtFile, delimiter = ';')
        gtReader.next()

        for index, row in enumerate(gtReader):
            image = plt.imread(_getTestDatasetPath(row[0]))

            if(adjustDatasetImages):
                image = _getAdjustedImage(image, imageWidth, imageHeight, row[3], row[4], row[5], row[6])
                plt.imsave(_getTestDatasetPath(row[0]), image)

            images.append(image[:,:,:3])
            labels.append(row[7])

    return (numpy.array(images), labels)

def load(adjustDatasetImages = False, imageWidth = 40, imageHeight = 40):
	for datasetPath in [ _getTestDatasetPath(), _getTrainingDatasetPath() ]:
		if not os.path.exists(datasetPath):
			os.makedirs(datasetPath)

	return _loadTrainingDataset(adjustDatasetImages, imageWidth, imageHeight), _loadTestDataset(adjustDatasetImages, imageWidth, imageHeight)