# -*- coding: utf-8 -*-

import os, keras
from keras.layers import Dense, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.models import Sequential
from keras.models import load_model

class GTSRBConvNet(keras.callbacks.Callback):

	def on_train_begin(self, logs={}):
		self.logs = []

	def on_epoch_end(self, batch, logs={}):
		self.logs.append(logs)

	def __init__(self, img_x, img_y, batch_size = 32, modelFilePath = False):
		self.num_classes = 43
		self.batch_size  = batch_size
		self.input_shape = (img_x, img_y, 3)

		if(not modelFilePath or not os.path.exists(modelFilePath)):
			self.model = Sequential()
			self.model.add(Conv2D(32, kernel_size = (5, 5), strides = (1, 1), activation = 'relu', input_shape = self.input_shape))
			self.model.add(MaxPooling2D(pool_size = (2, 2), strides = (2, 2)))
			self.model.add(Conv2D(64, (5, 5), activation = 'relu'))
			self.model.add(MaxPooling2D(pool_size = (2, 2)))
			self.model.add(Flatten())
			self.model.add(Dense(1000, activation = 'relu'))
			self.model.add(Dense(self.num_classes, activation = 'softmax'))

		else:
			self.model = load_model(modelFilePath)

	def train(self, x_train, y_train, x_validate, y_validate, epochs = 10, optimizer = keras.optimizers.Adam(lr = 0.001, beta_1 = 0.3, beta_2 = 3, epsilon = 1e-08, decay = 0.0), loss = keras.losses.categorical_crossentropy, callbacks = []):
		x_train    = x_train.astype('float32')
		x_validate = x_validate.astype('float32')
		x_train    = x_train / 255
		x_validate = x_validate / 255
		y_train    = keras.utils.to_categorical(y_train, self.num_classes)
		y_validate = keras.utils.to_categorical(y_validate, self.num_classes)

		callbacks = [self] + callbacks

		self.model.compile(loss = loss, optimizer = optimizer, metrics = ['accuracy'])
		hist = self.model.fit(x_train, y_train, batch_size = self.batch_size, epochs = epochs, verbose = 1, validation_data = (x_validate, y_validate), callbacks = callbacks)

		return (hist, self.logs)

	def export(self, modelFilePath = 'model.h5'):
		self.model.save(modelFilePath)

	def predict(self, x_test):
		x_test = x_test.astype('float32')
		x_test = x_test / 255
		predicted = self.model.predict(x_test, self.batch_size, 1)
		return predicted.tolist()